package me.rakibulmdalam.spring.crudapp.controllers;

import me.rakibulmdalam.spring.crudapp.entities.Category;
import me.rakibulmdalam.spring.crudapp.exceptions.*;
import me.rakibulmdalam.spring.crudapp.repositories.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@CrossOrigin
@RestController
@RequestMapping("/api/v1")
public class CategoryController {

    @Autowired
    private CategoryRepository categoryRepository;


    @GetMapping("/categories")
    public List<Category> getAllCategories() {
        return (List<Category>)categoryRepository.findAllActive();
    }


    @GetMapping("/category/{id}")
    public ResponseEntity<Category> getCategoriesById(@PathVariable(value = "id") Long categoryId)
            throws ResourceNotFoundException {
        Category category = categoryRepository
                .findById(categoryId)
                .orElseThrow(() -> new ResourceNotFoundException("Category not found on :: " + categoryId));
        return ResponseEntity.ok().body(category);
    }


    @PostMapping("/categories")
    public Category createProduct(@Valid @RequestBody Category category) {
        // System.out.println(product.getTitle());
        category.setDeleted(false);
        return categoryRepository.save(category);
    }


    @PutMapping("/category/{id}")
    public ResponseEntity<Category> updateCategory(
            @PathVariable(value = "id") Long categoryId, @Valid @RequestBody Category categoryDetails)
            throws ResourceNotFoundException {

        Category category = categoryRepository
                .findById(categoryId)
                .orElseThrow(() -> new ResourceNotFoundException("Category not found on :: " + categoryId));

        category.setName(categoryDetails.getName());
        category.setPath(categoryDetails.getPath());

        final Category updatedCategory = categoryRepository.save(category);
        return ResponseEntity.ok(updatedCategory);
    }


    @DeleteMapping("/category/{id}")
    public Map<String, Boolean> deleteCategory(@PathVariable(value = "id") Long categoryId) throws Exception {
        Category category = categoryRepository
                .findById(categoryId)
                .orElseThrow(() -> new ResourceNotFoundException("Category not found on :: " + categoryId));

        category.setUpdatedAt(new Date());
        category.setDeleted(true);

        final Category updatedCategory = categoryRepository.save(category);
        Map<String, Boolean> response = new HashMap<>();
        response.put("deleted", Boolean.TRUE);
        return response;
    }
}