package me.rakibulmdalam.spring.crudapp.controllers;

import me.rakibulmdalam.spring.crudapp.entities.Category;
import me.rakibulmdalam.spring.crudapp.entities.Product;
import me.rakibulmdalam.spring.crudapp.exceptions.*;
import me.rakibulmdalam.spring.crudapp.repositories.CategoryRepository;
import me.rakibulmdalam.spring.crudapp.repositories.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@CrossOrigin
@RestController
@RequestMapping("/api/v1")
public class ProductController {

    @Autowired
    private ProductRepository productRepository;
    @Autowired
    private CategoryRepository categoryRepository;

    @GetMapping("/products")
    public List<Product> getAllProducts() {
        return (List<Product>)productRepository.findAllActive();
    }


    @GetMapping("/products/{id}")
    public ResponseEntity<Product> getProductsById(@PathVariable(value = "id") Long productId)
            throws ResourceNotFoundException {
        Product product = productRepository
                            .findById(productId)
                            .orElseThrow(() -> new ResourceNotFoundException("Product not found on :: " + productId));
        return ResponseEntity.ok().body(product);
    }


    @PostMapping("/products")
    public Product createProduct(@Valid @RequestBody Product product)
            throws ResourceNotFoundException {

        Category category = categoryRepository
                .findById(product.getCategory_id())
                .orElseThrow(() -> new ResourceNotFoundException("Category id not found on :: " + product.getCategory_id()));

        product.setCategory(category);
        product.setCreatedAt(new Date());
        product.setUpdatedAt(new Date());
        product.setDeleted(false);
        return productRepository.save(product);
    }


    @PutMapping("/product/{id}")
    public ResponseEntity<Product> updateProduct(
            @PathVariable(value = "id") Long productId, @Valid @RequestBody Product productDetails)
            throws ResourceNotFoundException {

        Product product = productRepository
                            .findById(productId)
                            .orElseThrow(() -> new ResourceNotFoundException("Product not found on :: " + productId));

        product.setTitle(productDetails.getTitle());
        //product.setCategory(productDetails.getCategory());
        product.setPrice(productDetails.getPrice());
        product.setUpdatedAt(new Date());
        product.setDeleted(productDetails.getDeleted());

        final Product updatedProduct = productRepository.save(product);
        return ResponseEntity.ok(updatedProduct);
    }


    @DeleteMapping("/product/{id}")
    public Map<String, Boolean> deleteProduct(@PathVariable(value = "id") Long productId) throws Exception {
        Product product = productRepository
                            .findById(productId)
                            .orElseThrow(() -> new ResourceNotFoundException("Product not found on :: " + productId));

        product.setUpdatedAt(new Date());
        product.setDeleted(true);

        final Product updatedProduct = productRepository.save(product);
        Map<String, Boolean> response = new HashMap<>();
        response.put("deleted", Boolean.TRUE);
        return response;
    }

}