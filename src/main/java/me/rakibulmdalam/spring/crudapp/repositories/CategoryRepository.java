package me.rakibulmdalam.spring.crudapp.repositories;

import me.rakibulmdalam.spring.crudapp.entities.Category;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Collection;


@Repository
public interface CategoryRepository extends JpaRepository<Category, Long> {
    @Query(value = "SELECT c FROM Category c WHERE isDeleted = false")
    Collection<Category> findAllActive();

    @Query(value = "SELECT c FROM Category c WHERE name = ?1 AND isDeleted = false")
    Collection<Category> findByName(String name);
}