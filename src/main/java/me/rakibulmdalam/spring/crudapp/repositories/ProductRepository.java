package me.rakibulmdalam.spring.crudapp.repositories;

import me.rakibulmdalam.spring.crudapp.entities.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Collection;
//import java.util.List;


@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {
    @Query(value = "SELECT p FROM Product p INNER JOIN Category c ON p.category = c WHERE p.isDeleted = false AND c.isDeleted = false")
    Collection<Product> findAllActive();
}